#
# database.sh
#
# Fucntions to work with db backups
#

DVTLS_DB_BACKUP_PREFIX="db_backup"

##########################################################
# Public commands
##########################################################
backup-db() {
	local BACKUP_DB_PATH=$( backup_path $1 )
	local DB_PATH=$( db_path )

	if [[ $1 ]]
	then
		echo "Saving this under the name: $1"
	else
		echo "Performing standard backup"
	fi

	rm -r "$BACKUP_DB_PATH"
	cp -r "$DB_PATH" "$BACKUP_DB_PATH" 
}


clear-db() {
	local DB_PATH="$DVTLS_DB_FOLDER/db"
	rm -f "$DB_PATH"/*.{mv.db,trace.db}
}

list-dbs() {
	# Print out everything in the db folder that's preceded by
	# the db backup prefix, and followed by a '/'.
	ls "$DVTLS_DB_FOLDER" | grep -oP "(?<=$DVTLS_DB_BACKUP_PREFIX-).*(?=/)"
}

restore-db() {
	local DB_PATH=$( db_path )
	local BACKUP_PATH=$( backup_path $1 )

	rm -rf "$DB_PATH"
	cp -r "$BACKUP_PATH" "$DB_PATH"
}

##########################################################
# Helper Functions
##########################################################
backup_path() {
	local BASE_BACKUP_PATH="$DVTLS_DB_FOLDER/$DVTLS_DB_BACKUP_PREFIX"

	if [[ $1 ]]
	then
		echo "$BASE_BACKUP_PATH"-"$1"
	else
		echo "$BASE_BACKUP_PATH"
	fi
}

db_path() {
	echo "$DVTLS_DB_FOLDER/db"
}


##########################################################
# Autocomplete
##########################################################
__db_autocomplete() {
	local BACKUPS=`list-dbs`

	local cur prev
	COMPREPLY=()
	cur="${COMP_WORDS[COMP_CWORD]}"
	COMPREPLY=( $(compgen -W "${BACKUPS}" -- ${cur}) )

	return 0
}
complete -o nospace -F __db_autocomplete restore-db
complete -o nospace -F __db_autocomplete backup-db
