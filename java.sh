# Modify the PATH and JAVA_HOME variables
# to point to the java 8 installation.
jdk8 () {
	#TODO: add this to user_config.sh
	java_8_path='/c/development/tools/jdk1.8.0_191'
	export JAVA_HOME="$java_8_path"
	export PATH="$java_8_path/bin:$PATH"
}


# Modify the PATH and JAVA_HOME variables
# to point to the java 11 installation.
jdk11 () {
	#TODO: add this to user_config.sh
	java_11_path='/c/Program Files/Java/jdk-11.0.4.11-hotspot'
	export JAVA_HOME="$java_11_path"
	export PATH="$java_11_path/bin:$PATH"
}
