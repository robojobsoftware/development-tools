#
# autocomplete.sh
#
# Autocomplete functions for scripts in the bin folder
# 



__transfer_autocomplete() {
	local cur prev projects
	people=`ls //alu-srv-file01/data_robocopy/Transfer | grep -Ev "(\.|\s)"`

	COMPREPLY=()
	cur="${COMP_WORDS[COMP_CWORD]}"
	COMPREPLY=( $(compgen -W "${people}" -- ${cur}) )

	return 0
}
complete -F __transfer_autocomplete transfer


__remote-desktop_autocomplete() {
	local cur prev projects
	connections=`ls $SCRIPT_DIR_PATH/remote_desktops | grep -Po "^\w+(?=\.)"`

	COMPREPLY=()
	cur="${COMP_WORDS[COMP_CWORD]}"
	COMPREPLY=( $(compgen -W "${connections}" -- ${cur}) )

	return 0
}
complete -F __remote-desktop_autocomplete remote-desktop
